<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>S01: PHP Basics and Selection Control Structure</title>
	</head>
	<body>
		<h1>Full Address</h1>
		<p><?php echo getFullAddress("Philippines", "Metro Manila", "Quezon City", "3F Caswynn Bldg., Timog Avenue"); ?></p>
		<p><?php echo getFullAddress("Philippines", "Metro Manila", "Makati City", "3F Enzo Bldg., Buendia Avenue"); ?></p>

		<h1>Letter-Based Grading</h1>
		<p><?php echo $grade1 ?> is equivalent to <?php echo getLetterGrade($grade1); ?></p>
		<p><?php echo $grade2 ?> is equivalent to <?php echo getLetterGrade($grade2); ?></p>
		<p><?php echo $grade3 ?> is equivalent to <?php echo getLetterGrade($grade3); ?></p>

	</body>
</html>